package com.greatlearning.designpattern1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SingletonDBConnection {
    // Static variable reference of single_db_instance of type single_db_instance
    private static SingletonDBConnection single_db_instance = null;
 
    // Declaring a variable of type Connection
    Connection connection = null;
 
    // We will be creating private constructor
    private SingletonDBConnection() throws SQLException
    {
    	connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "admin");
    }
 
    // Static method to create instance of Singleton class
    public static SingletonDBConnection getInstance() throws SQLException
    {
        if (single_db_instance == null)
        	single_db_instance = new SingletonDBConnection();
 
        return single_db_instance;
    }
}


