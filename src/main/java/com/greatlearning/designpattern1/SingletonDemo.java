package com.greatlearning.designpattern1;

import java.sql.SQLException;

public class SingletonDemo {

	public static void main(String[] args) throws SQLException {

		// Instantiating SingletonDBConnection class with variable db1
		SingletonDBConnection db1 = SingletonDBConnection.getInstance();

		// Instantiating SingletonDBConnection class with variable db2
		SingletonDBConnection db2 = SingletonDBConnection.getInstance();

		// Printing the memory address for above variable
		System.out.println("Memory address of db1 is " + db1);
		System.out.println("Memory address of db2 is " + db2);

		if (db1 == db2) {

			System.out.println("db1 and db2 objects point to the same memory address, so same object");
		}

	}

}
