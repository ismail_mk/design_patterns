package com.greatlearning.designpattern3;

// Context class with convert method
public class ConverterContext {
	IConverterStrategy converterStrategy;
	public GBPConverter setConverterStrategy;

	public void setConverterStrategy(IConverterStrategy converterStrategy) {
		this.converterStrategy = converterStrategy;
	}

	public double convert(double amount) {
		return converterStrategy.convertToINR(amount);
	}

}
