package com.greatlearning.designpattern3;

// GBPConverter class
public class GBPConverter implements IConverterStrategy {
	IConverterStrategy converter;

	public void setGBPConverter(IConverterStrategy converter) {
		this.converter = converter;
	}

	public double convertToINR(double amount) {
		double gbpExchangeValue = 80.37737;
		return amount * gbpExchangeValue;

	}
}
