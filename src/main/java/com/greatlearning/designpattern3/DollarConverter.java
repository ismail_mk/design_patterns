package com.greatlearning.designpattern3;

//DollarConverter class
public class DollarConverter implements IConverterStrategy{
	IConverterStrategy converter;

	public void setDollarConverter(IConverterStrategy converter) {
		this.converter = converter;
	}

	public double convertToINR(double amount) {
		double usdExchangeValue;
		usdExchangeValue = 75.37737;
		return amount * usdExchangeValue;
		
	}

}
