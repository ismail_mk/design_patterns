package com.greatlearning.designpattern3;

public interface IConverterStrategy {
	public double convertToINR(double amount);

}
