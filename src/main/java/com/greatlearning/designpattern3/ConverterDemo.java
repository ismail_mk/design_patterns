package com.greatlearning.designpattern3;

public class ConverterDemo {

	public static void main(String[] args) {
		ConverterContext context = new ConverterContext();
        
        // Setting GBPConverter strategy
        context.setConverterStrategy( new GBPConverter());
        double gbpAmount= context.convert(1000);
 
        System.out.println("(GBP->INR) Amount: " + gbpAmount);
        System.out.println("===============================");
 
        // Setting Dollar strategy
        context.setConverterStrategy(new DollarConverter());
        double usdAmount= context.convert(2000);
 
        System.out.println("(USD->INR) Amount: " + usdAmount);
        System.out.println("================================");

	}

}
