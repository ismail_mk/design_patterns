package com.greatlearning.designpattern2;

public class Bank {
	private String bankAccountNo;
	private String accountType;
	private String branch;
	private double balance;

	private String atmTransactions;
	private String emiSchedule;

	// All args Constructor
	private Bank(BankBuilder bankBuilder) {
		this.bankAccountNo = bankBuilder.bankAccountNo;
		this.accountType = bankBuilder.accountType;
		this.branch = bankBuilder.branch;
		this.balance = bankBuilder.balance;
		this.atmTransactions = bankBuilder.atmTransactions;
		this.emiSchedule = bankBuilder.emiSchedule;

	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getAtmTransactions() {
		return atmTransactions;
	}

	public void setAtmTransactions(String atmTransactions) {
		this.atmTransactions = atmTransactions;
	}

	public String getEmiSchedule() {
		return emiSchedule;
	}

	public void setEmiSchedule(String emiSchedule) {
		this.emiSchedule = emiSchedule;
	}

	@Override
	public String toString() {
		return "Bank [bankAccountNo=" + bankAccountNo + ", accountType=" + accountType + ", branch=" + branch
				+ ", balance=" + balance + ", atmTransactions=" + atmTransactions + ", emiSchedule=" + emiSchedule
				+ "]";
	}

	// Static builder class
	public static class BankBuilder {

		private String bankAccountNo;
		private String accountType;
		private String branch;
		private Double balance;

		@SuppressWarnings("unused")
		private String atmTransactions;
		@SuppressWarnings("unused")
		private String emiSchedule;

		// constructor
		public BankBuilder(String bankAccountNo, String accountType, String branch, double balance) {

			this.bankAccountNo = bankAccountNo;
			this.accountType = accountType;
			this.branch = branch;
			this.balance = balance;
		}

		public BankBuilder atmTransactions(String atmTransactions) {
			this.atmTransactions = atmTransactions;
			return this;
		}

		public BankBuilder emiSchedule(String emiSchedule) {
			this.emiSchedule = emiSchedule;
			return this;
		}

		public Bank build() {
			return new Bank(this);
		}
	}

}
