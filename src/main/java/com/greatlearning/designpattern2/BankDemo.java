package com.greatlearning.designpattern2;

public class BankDemo {

	public static void main(String[] args) {
		
		Bank user1 = new Bank.BankBuilder("A001", "Savings", "Chennai", 1000)
				// no atm transactions
				// no emi schedules
				.build();

		System.out.println(user1);

		Bank user2 = new Bank.BankBuilder("A002", "Current", "Bangalore", 0)
				.atmTransactions("Yes")
				// no emi schedules
				.build();

		System.out.println(user2);

		Bank user3 = new Bank.BankBuilder("A003", "Savings", "Bangalore", 200000)
				.atmTransactions("Yes")
				.emiSchedule("Yes")
				.build();

		System.out.println(user3);
	}

}
